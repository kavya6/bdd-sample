Feature: iSelect-Energy    

Scenario: Home page transition
    Given User opens URL "https://admin-demo.nopcommerce.com/login?ReturnUrl=%2Fadmin%2F"
    When User enters email as "admin@yourstore.com" and password as "admin"
    And Click on login button
    Then Page title should be "Dashboard / nopCommerce administration"

