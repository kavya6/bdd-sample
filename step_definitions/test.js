const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');


Given(/^User opens URL "(.*?)"$/, (url) => {
    return client
      .url(url)
      .waitForElementVisible('body', 7000);
  });

  When('User enters email as {string} and password as {string}', function (string, string2) {
    return client.saveScreenshot('test_ss/loginpage.png');
  });

  When('Click on login button', function () {
    return client.click('input[type="submit"]')
    .saveScreenshot('test_ss/submitclick.png');
  });

  Then(/^Page title should be "(.*?)"$/,(title2)=>{
    return client.saveScreenshot('test_ss/page2.png')
    .assert.title(title2);
});
